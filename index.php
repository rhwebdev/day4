<?php
   error_reporting(E_ALL);
   ini_set('display_errors', 1);

   $no = 2;
   $text = 'hello';
   $person = "tom";
   $char = "4";
   $float = 3.3;
   $boo = false;
   $ar = ['a', 'b', 'c'];
   
   var_dump($no, $text, $char, $float, $boo);
   echo "<br>--------------------------<br>";

   echo "string length is = ".strlen($text);
   echo "<br>--------------------------<br>";
   echo $text." ".$person." ... !";
   echo "<br>--------------------------<br>";


   echo $ar[2];
   echo "<br>--------------------------<br>";

   print_r($ar);
   echo "<br>--------------------------<br>";

   for ($i=0; $i < sizeof($ar) ; $i++) { 
      // if($i == 0){
      //    echo "dont print <br>";
      // }
      // else{
      //    echo 'Index '.$i.' of array is '.$ar[$i].'<br>';
      // }

      if($i > 0) echo 'Index '.$i.' of array is '.$ar[$i].'<br>';
   }

   echo "<br>--------------------------<br>";
   
   foreach ($ar as $index => $val) {
      echo "index ".$index." = ".$val."<br>";
   }
   echo "<br>--------------------------<br>";

   $numbers = array();
   print_r($numbers);
   $a = 1;
   while ($a <= 10) {
      array_push($numbers, $a);
      $a++;
   }
   print_r($numbers);
   echo "<br>--------------------------<br>";
   echo "<pre>"; print_r(array_merge($ar, $numbers)); echo "</pre>";
   echo "<br>--------------------------<br>";

   function sum($x){
      // global $no;
      $y = 1;
      $sum = $x+ $GLOBALS['no'] + $y;
      $y = 10;
      return $sum;
   }

   $total = sum(20);
   var_dump($total);

   echo "<br>--------------------------<br>";

   function increase(){
      static $y = 10;
      $y++;
      return $y;
   }
   
   
   print increase();
   echo "<br>--------------------------<br>";
   echo increase();
   echo "<br>--------------------------<br>";
   echo increase();
   echo "<br>--------------------------<br>";
   echo increase();
   echo "<br>--------------------------<br>";
   
?>